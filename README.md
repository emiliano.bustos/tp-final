# README



## Descripción

En este readme se explicara la función de los archivos, scripts, dockerfile etc. de este proyecto y como realizar el deploy.



## Contenido del proyecto "TP **FINAL**"

- DockerFile
- entrypoint.sh
- Index.html
- Default
- readme.md

## DockerFile

Indicamos la imagen base a utilizar para desplegar la imagen

```
FROM ubuntu:latest
```

"LABEL" es una etiqueta que ponemos la version de la app

```
LABEL version="1"
```

Etiquetamos con una breve descripción

```
LABEL description="aplicacion Final" 
```



Es es una variable de entorno para indicar que el despliegue va a ser sin interacción con el usuario

```
ARG DEBIAN_FRONTEND=noninteractive 
```

Instruccion para que el dockerfile primero corra un update de los repositorios y luego instale nginx

```
RUN     apt update && apt install nginx -y  
```

ADD (similar al comando CP) copiara desde una URL el archivo en este caso index.html y a  /var/www/html 

```
ADD https://gitlab.com/emiliano.bustos/tp-final/-/raw/main/index.html  /var/www/html  
```

Luego con RUN vamos a dar permisos sobre la carpeta para que nginx tenga permisos de lectura solamente

```
RUN chmod +r /var/www/* -R  
```

Luego se trae el archivo default desde el repositorio que es el Vhost y lo copia a /etc/nginx/sites-available (esto es dentro del contenedor como en el paso anterior.)

```
ADD https://gitlab.com/emiliano.bustos/tp-final/-/raw/main/default /etc/nginx/sites-available 
```

Con ADD nuevamente traemos el entrypoint.sh al directorio raiz

```
ADD https://gitlab.com/emiliano.bustos/tp-final/-/raw/main/entrypoint.sh /
```

En esta línea damos permisos de ejecución al entrypoint.sh

```
RUN chmod +x /entrypoint.sh
```

Con EXPOSE indicamos que puertos expondremos al contenedor

```
EXPOSE  80 443 
```

Acá indicamos que levante un script de ejecución

```
ENTRYPOINT ["/entrypoint.sh"]
```

Al haber un script de ejecución (el entrypoint) se pasa como argumento 

```
CMD ["nginx", "-g", "daemon off;"]
```





## Entrypoint.sh

Indicamos el interprete que va a utilizar para leer el archivo

```
#!/bin/bash 
```

Si el scrip tiene un error se cancela

```
Set -e 
```

Para cambiar el titulo de la pagina, reemplaza dentro del archivo index.html la línea “TITULO” por la que indiquemos en la variable de entorno, la sentencia quedaría: ` -e TITULO="TITULO QUE NECESITAMOS QUE TENGA LA PAGINA" `

```
sed -i 's/TITULO/'"${TITULO}"'/' /var/www/html/index.html
```



Para cambiar el color de fondo en la pagina, reemplza dentro del archivo index.html la línea “COLOR” por el código ASCII que indiquemos en la variable de entorno, la sentencia quedaría: ` -e COLOR=#FF0000` (sin COMILLAS)

**ejemplo de colores en https://www.cdmon.com/es/tabla-colores**

```
sed -i 's/COLOR/'${COLOR}'/' /var/www/html/index.html 
```



## DEFAULT

Es el Vhost con el contenido estándar se incluye cuando se despliga en el DockerFile.



## index.html

Es el contenido HTML estático de la pagina web que se despliegan, dentro esta preparado para poder ser modificado con las variables de entorno antes mencionadas.



## Construcción de la Imagen

1-   Clonar el repositorio: 

```
# git clone https://gitlab.com/emiliano.bustos/tp-final.git
```

2-   Crear la imagen a partir del dockerfile obtenido durante la clonación (argumento -t es para indicar el tag de la imagen, el nombre básicamente): 

```
# docker build -t tp-final .
```

3-   Corremos el contenedor apartir de la imagen creada, indicando las variables para el titulo de la pagina y el color de fondo:  

```
# docker run -d -p 80:80 -e TITULO="EMILIANO" -e COLOR=#00FF00 --name tp-final tp-final
```

4-   Confirmamos el correcto funcionamiento verificando que esta corriendo con el comando: 

```
# docker ps
```



Verificamos con un explorador web, acciendo con la ip y el puerto donde corre el container y verificamos su correcto funcionamiento




