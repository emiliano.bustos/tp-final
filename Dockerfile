#!/bin/bash
FROM ubuntu:latest
LABEL version="1"
LABEL description="aplicacion Final"

ARG DEBIAN_FRONTEND=noninteractive


RUN	apt update && apt install nginx -y

ADD https://gitlab.com/emiliano.bustos/tp-final/-/raw/main/index.html  /var/www/html
RUN chmod +r /var/www/* -R
ADD https://gitlab.com/emiliano.bustos/tp-final/-/raw/main/default /etc/nginx/sites-available
ADD https://gitlab.com/emiliano.bustos/tp-final/-/raw/main/entrypoint.sh /
RUN chmod +x /entrypoint.sh



EXPOSE 	80 443

ENTRYPOINT ["/entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]

